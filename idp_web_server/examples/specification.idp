vocabulary environment {
    nr_of_participants: () → ℤ

    [short:Applies to all participants]
    have_Covid_Safe_Ticket: () → 𝔹

    [short:Applies to all participants]
    have_masks: () → 𝔹
}
vocabulary decision {
    import environment

    type Activity := {Outdoor_sport, Indoor_sport, Restaurant, Cinema, Theater, None}
    activity: () → Activity

    [short:Hour of the day when the activity ends]
    end_time: () → ℤ
}
theory environment:environment {
    [Nr of participants is positive]
    1 ≤ nr_of_participants().
}
theory decision:decision {

    [Outdoor sport activity must end at 8 PM]
    activity() = Outdoor_sport ⇒ [ends at 8 PM](end_time() ≤ 8)
            ∧ ( have_masks() ∨ have_Covid_Safe_Ticket() ).

    [Indoor sport activity must end at 8 PM and participants must have the Covid Safe Ticket]
    activity() = Indoor_sport ⇒ end_time() ≤ 8 ∧ have_Covid_Safe_Ticket().

    [At most 6 persons, all with Covid Safe Ticket, can sit at a restaurant table, up to 11 PM]
    activity() = Restaurant ⇒ [ends at 11 PM](end_time() ≤ 11) ∧
                              [6 participants or less] nr_of_participants() ≤ 6 ∧
                              have_Covid_Safe_Ticket().

    [Cinemas and Theater can sit at most 2 people next to each other, all with Covid Safe Ticker, and must close at 11 PM]
    ([Cinema or Theater] activity() ∈ {Cinema, Theater}) ⇒ end_time() ≤ 11 ∧
                    [1 or 2 participants] nr_of_participants() ≤ 2 ∧
                    have_Covid_Safe_Ticket().

    [Activities end in the afternoon]
    1 ≤ end_time() ≤ 12.
}

display {
    heading('Participants', `nr_of_participants, `have_Covid_Safe_Ticket, `have_masks).
    heading('Activity ', `activity, `end_time).
    view() = expanded.
    unit('PM', `end_time).
}

