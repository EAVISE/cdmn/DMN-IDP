vocabulary environment {
    social_habitat : () → 𝔹
    licensed_seller : () → 𝔹
    low_rent : () → 𝔹

}

vocabulary decision {
    import environment

    type Registration_Type := {Social, Modest, Other}
    type Tax_Rate := {1, 7, 10}

    tax_rate : () → Tax_Rate
    registration_Type : () → Registration_Type

}

theory environment:environment {
    // theory of possible situations

    social_habitat() ⇒ low_rent().
}

theory decision:decision {
    // legislation

    { tax_rate() = 1  ← registration_Type() = Social.
      tax_rate() = 7  ← registration_Type() = Modest.
      tax_rate() = 10 ← registration_Type() = Other.
    }

    registration_Type() = Social ⇒ licensed_seller() ∧ social_habitat().
    registration_Type() = Modest ⇒ low_rent().

}

display {
    expand := {`tax_rate}.
    goal_symbol := {`tax_rate, `registration_Type}.
    view() = normal.
}