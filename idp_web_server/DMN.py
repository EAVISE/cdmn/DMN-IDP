"""
    Copyright 2020 Simon Vandevelde

    This file is part of Interactive_Consultant_DMN

    Interactive_Consultant is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Interactive_Consultant is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Interactive_Consultant.  If not, see <https://www.gnu.org/licenses/>.
"""
import subprocess
import re
from idp_web_server.Inferences import oppositeOperator, refineIntCons, reverseOperator, combine_rules
from idp_web_server.State import State
import idp_web_server.rest
from idp_engine import IDP
import time
import pandas as pd
import json


"""
#################
# Operations on DMN tables.
#################
"""


def run_cdmn_solver(options=[]):
    """
    Runs the cDMN solver via a pipe.
    This is not the best implementation.
    TODO: write a clean interface between the two.

    :arg options: the list of options.
    :returns stderr, stdout.  """
    # TODO: write a clean interface.
    args = ["cdmn", "./spec.dmn",
            "--interactive-consultant-idp-z3"] + options
    proc = subprocess.Popen(args,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    proc.wait()  # Wait for the process to finish
    # Print stderr, stdout.
    stderr, stdout = proc.communicate()
    stderr = stderr.decode()
    stdout = stdout.decode()
    return stderr, stdout


def add_background(idp: str, background: str) -> str:
    """
    Add background knowledge to IDP code.
    """
    if background:
        idp = idp.replace("theory T: V{", "theory T: V{\n" + background)
    return idp


def DMN_to_Z3(xml: str, background: str) -> str:
    """
    Function to convert a DMN XML specification into an IDPz3 specification.

    :arg xml: the stringified version of the DMN XML.
    :returns str: the IDPz3 spec.
    """
    with open("spec.dmn", "w") as fp:
        fp.write(xml)
    stderr, stdout = run_cdmn_solver(["-o", "spec.idp"])
    if "Error" in stderr:
        return stderr
    spec = ""
    with open("spec.idp", "r") as fp:
        spec = fp.read()
    return add_background(spec, background)


def DMN_check_overlap(args, runIDP) -> str:
    """
    Function to check DMN tables for overlap.

    :arg args: the arguments of specification to check.
    :arg runIDP: a function with which IDPz3 can be run.
    :returns str: a string containing the result of the check.
    """
    if not args['active']:
        return "No table selected."
    else:
        # The table to error check.
        table_output = args['active']
    # Write the XML code to a file.
    xml = args['code']
    with open("spec.dmn", "w") as fp:
        fp.write(xml)
    stderr, stdout = run_cdmn_solver(['-o', 'spec.idp',
                                      '--errorcheck-overlap', table_output])
    with open("spec.idp", "r") as fp:
        spec = fp.read()
    spec = add_background(spec, args['background'])

    # Restructure the args to modelexpand.
    args['method'] = 'modelexpand'
    args['code'] = spec
    args['active'] = None
    args['value'] = None

    # Now execute the IDP file.
    try:
        IDP = runIDP(args)
        fired = []
        for r, valdict in IDP['Rule_row'].items():
            try:
                if valdict['value'] is True:
                    val = r.split('(')[1].split(')')[0]
                    val = str(int(val) + 1)
                    fired.append(val)
            except Exception:
                continue
        rules = ",".join(fired)
        return ("Input overlap in rows {}!"
                .format(rules))
    except Exception as e:
        if 'Not satisfiable' in str(e):
            return "Table is correct."
        else:
            raise(e)
            return "Overlap: Something went wrong. {}".format(e)


def DMN_check_shadowed(args, runIDP) -> str:
    """
    Function to check DMN tables for shadowed rules.

    :arg args: the arguments of specification to check.
    :arg runIDP: a function with which IDPz3 can be run.
    :returns str: a string containing the result of the check.
    """
    try:
        if not args['active']:
            return "No table selected."
        else:
            # The table to error check.
            table_output = args['active']

        # Write the XML code to a file.
        xml = args['code']
        with open("spec.dmn", "w") as fp:
            fp.write(xml)

        # Generate a single test file and add the background knowledge.
        stdouts = []
        _, stdout = run_cdmn_solver(["-o", "spec.idp",
                                     "--errorcheck-shadowed", table_output])
        stdouts.append(stdout)

        with open("spec.idp", "r") as fp:
            spec = fp.read()
        spec = add_background(spec, args['background'])

        # Now execute the IDP file.
        args['method'] = 'propagate'
        args['code'] = spec
        args['active'] = None
        args['with_relevance'] = False
        idp_output = runIDP(args)['Rule_row']

        # We need to search the IDP output for each Rule_row with
        # `status=CONSEQUENCE` and `value=False`
        shadowed_list = []
        for name, value in idp_output.items():
            if not isinstance(value, dict):
                continue
            if value['status'] != 'UNKNOWN' and not value['value']:
                idx = re.findall(r'\((.*)\)', name)[0]
                shadowed_list.append(str(int(idx) + 1))

        if len(shadowed_list) > 1:
            shadowed_str = ", ".join(shadowed_list)
            return "Rules {} are shadowed".format(shadowed_str)
        elif len(shadowed_list) == 1:
            return "Rule {} is shadowed".format(shadowed_list[0])
        else:
            return "Table is correct."
    except Exception as e:
        raise Exception(f"Unfireable: {str(e)}")
        return f"Unfireable: Something went wrong. {e}"


def DMN_check_gap(args, runIDP):
    """
    Function to check DMN tables for input gap errors.

    :arg args: the arguments of specification to check.
    :arg runIDP: a function with which IDPz3 can be run.
    :returns str: a string containing the result of the check.
    """
    if not args['active']:
        return "No table selected."
    else:
        # The table to error check.
        table_output = args['active']
    # Write the XML code to a file.
    xml = args['code']
    with open("spec.dmn", "w") as fp:
        fp.write(xml)

    inputs = args['expanded']
    run_cdmn_solver(["-o", "spec.idp",
                     "--errorcheck-gap", table_output])

    with open("spec.idp", "r") as fp:
        spec = fp.read()
    spec = add_background(spec, args['background'])

    # Restructure the args to modelexpand.
    args['method'] = 'modelexpand'
    args['code'] = spec
    args['active'] = None
    args['value'] = None

    # Now execute the IDP file.
    try:
        # If this succeeds, there is a gap in the table.
        runIDP(args)
        # Now that we know that there's a gap, we want to do a more thorough
        # check to actually find the missing rules.
        # To do this, we need to make some changes to the spec first.
        # We change the rule_row() predicates by a conjunctive equivalence defining correct()
        # and delete all unwanted curly brackets
        spec = re.sub(r"\!row in row_t: true => ~\(Rule_row\(row\)\).", "", spec)
        spec = re.sub(r"NbRules:  -> Int", "", spec)
        spec = re.sub(r"(type row_t := {)(.*?)\..(.*)}", "", spec)
        spec = re.sub(r"\s*Rule_row\(0\) <-", "correct() <=>", spec)
        spec = re.sub(r"(Rule_row: row_t -> Bool)", "correct: () -> Bool", spec)
        spec = re.sub(r"(Rule_row: row_t → Bool)", "correct: () -> Bool", spec)
        spec = re.sub(r"\.\n\s*Rule_row\(.*?\) <-", "|", spec)
        spec = re.sub(r"}\n*\s*{", "\n", spec)
        spec = re.sub(r"}\s*\n*}", "}", spec)
        spec = re.sub(r"<- true", "", spec)
        spec = re.sub(r"//Fired rule\n+\s*{", "", spec)

        # After the initial changes, we transform the table definitions into equivalences
        matches = re.findall(r"(\w*)\(\) = (\w*) <-", spec)
        prev = ''
        for match in matches:
            if prev != match:
                reg1 = str(match[0]) + r"\(\) = " + str(match[1]) + r" <-"
                reg2 = str(match[0]) + r"() = " + str(match[1]) + r" <=>"
                spec = re.sub(reg1, reg2, spec, 1)
            else:
                reg1 = r"\.\n\s*" + str(match[0]) + r"\(\) = " + str(match[1]) + r" <-"
                spec = re.sub(reg1, " |", spec)
            prev = match

        # Now we can perform abstract model generation.
        idp = IDP.from_str(spec)
        state = State.make(idp, "{}", "{}")
        table=state.decision_table(goal_string="",
                             timeout=time.time() + 20,
                             max_rows=50,
                             first_hit=False)

        # We ne refine the decision table to clearly show the gap
        # To do this we combine all rules defining when correct() is false in a dataframe
        gap = pd.DataFrame(columns=args['expanded'])
        i = 0
        for rule in table:
            correct = True
            new_rule = []
            for assign in rule:
                if assign.symbol_decl != None:
                    if str(assign.symbol_decl.name) == "correct" and str(assign.value) == "false":
                        correct = False
                    else:
                        new_rule.append(assign)

            # If a not correct() appears in the rule, in represents a gap in the DMN table and should be added
            if not correct:
                df = pd.DataFrame(index=[i],columns=args['expanded'])
                for assign in new_rule:
                    name = str(assign.symbol_decl.name)

                    # Look whether the symbol or the value comes first
                    if str(assign.sentence.sub_exprs[1])[:-2] == name:
                        value = str(assign.sentence.sub_exprs[0])
                        operator = oppositeOperator(str(assign.sentence.operator[0]))
                    else:
                        value = str(assign.sentence.sub_exprs[1])
                        operator = str(assign.sentence.operator[0])

                    # If the value of the assignment is False reverse the operator
                    if str(assign.value) == "false":
                        operator = reverseOperator(operator)

                    # Only variables that are inputs of the table are included in our gap table
                    if name in gap.columns:
                        if str(df.loc[i][name]) == "nan":
                            df.loc[i][name] = [operator, value]
                        else:
                            refined = refineIntCons(operator, float(value), df.loc[i][name][0], float(df.loc[i][name][1]))
                            df.loc[i][name] = refined
                i += 1
                gap = pd.concat([gap,df], ignore_index=True)

        # TODO Look of we can combine any rules
        # gap = combine_rule(gap)

        # Now format the pandas df as a sensible table (= list of lists)
        gap_values = gap.values.tolist()
        table = [list(gap)]  # Set the header of the table.
        for values in gap_values:
            formatted_values = [' '.join(x) for x in values]
            table.append(formatted_values)
        return "Gap Dectected:", table
        # return 'Table is correct.', []
    except Exception as e:
        if 'Not satisfiable' in str(e):
            return "Table is correct.", []
        else:
            raise Exception(e)
            return "Gap: Something went wrong. {}".format(e), []
