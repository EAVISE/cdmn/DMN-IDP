# Copyright 2019 Ingmar Dasseville, Pierre Carbonnelle
#
# This file is part of Interactive_Consultant.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This module contains the logic for inferences
that are specific for the Interactive Consultant.
"""

import time

from idp_engine.Assignments import Status as S
from idp_engine.Expression import (AppliedSymbol, TRUE, Number, Expression, AQuantification,
                                   AConjunction, Brackets,AMultDiv)
from idp_engine.utils import OrderedSet
from .IO import Output
import pandas as pd


def explain(state, consequence=None):
    (facts, laws) = state.explain(consequence)
    out = Output(state)
    for ass in facts:
        out.addAtom(ass.sentence, ass.value, ass.status)
    out.m['*laws*'] = [(law.code, law.annotations['reading']) for law in laws]

    # remove irrelevant atoms
    for symb, dictionary in out.m.items():
        if symb != '*laws*':
            out.m[symb] = {k: v for k, v in dictionary.items()
                            if type(v) == dict and v['status'] in
                               ['GIVEN', 'STRUCTURE', 'DEFAULT', 'EXPANDED']
                            and v.get('value', '') != ''}

    out.m = {k: v for k, v in out.m.items() if v or k == '*laws*'}
    return out.m


def abstract(state, given_json):
    timeout, max_rows = 20, 50
    max_time = time.time()+timeout
    models = state.decision_table(goal_string="", timeout=timeout, max_rows=max_rows,
                        first_hit=False)
    # detect symbols with assignments
    table, active_symbol = {}, {}
    for i, model in enumerate(models):
        for ass in model:
            if (ass.sentence != TRUE
            and ass.symbol_decl is not None):
                active_symbol[ass.symbol_decl.name] = True
                if (ass.symbol_decl.name not in table):
                    table[ass.symbol_decl.name]= [ [] for i in range(len(models))]
                table[ass.symbol_decl.name][i].append(ass)

    # build table of models
    out = {} # {heading : [Assignment]}

    out["universal"] = list(l for l in state.assignments.values()
                            if l.status == S.UNIVERSAL)
    out["given"    ] = list(l for l in state.assignments.values()
                            if l.status in [S.GIVEN, S.DEFAULT, S.EXPANDED])
    # TODO: separate field for S.DEFAULT or S.EXPANDED?
    out["fixed"    ] = list(l for l in state.assignments.values()
                            if l.status in [S.ENV_CONSQ, S.CONSEQUENCE])
    out["irrelevant"]= list(l for l in state.assignments.values()
                            if l.status not in [S.ENV_CONSQ, S.CONSEQUENCE]
                            and not l.relevant)

    out["models"] = ("" if len(models) < max_rows and time.time()<max_time else
        f"Time out or more than {max_rows} models...Showing partial results")
    out["variable"] = [[ [symb] for symb in table.keys()
                        if symb in active_symbol ]]
    for i in range(len(models)):
        out["variable"] += [[ table[symb][i] for symb in table.keys()
                            if symb in active_symbol ]]
    return out


def mergeIntCons(operator, value, nextOperator, nextValue):
    if operator and nextOperator in [">","≥"]:
        if value > nextValue:
            return [nextOperator, str(nextValue)]
        elif value < nextValue:
            return [operator,value]
        elif value == nextValue and (operator=="≥" or nextOperator=="≥"):
            return ["≥",value]
        else:
            return [operator,value]
    elif operator and nextOperator in ["<", "≤"]:
        if value > nextValue:
            return [operator, value]
        elif value < nextValue:
            return [nextOperator,str(nextValue)]
        elif value == nextValue and (operator == "≥" or nextOperator == "≥"):
            return ["≥",value]
        else:
            return [operator,value]
    elif operator in ["<", "≤"] and nextOperator in [">","≥"]:
        if value >= nextValue:
            return ["=","-"]
        elif operator and nextOperator in ["≤","≥"]:
            return [["[","]"],[nextValue,value]]
        elif operator and nextOperator in ["<",">"]:
            return [["(", ")"], [nextValue, value]]
        elif operator=="≤" and nextOperator==">":
            return [["(", "]"], [nextValue, value]]
        elif operator=="<" and nextOperator=="≥":
            return [["[", ")"], [nextValue, value]]
    elif operator in [">", "≥"] and nextOperator in ["<", "≤"]:
        if value < nextValue:
            return ["=","-"]
        elif operator and nextOperator in ["≤","≥"]:
            return [["[","]"],[value,nextValue]]
        elif operator and nextOperator in ["<",">"]:
            return [["(", ")"], [value, nextValue]]
        elif operator==">" and nextOperator=="≤":
            return [["(", "]"], [value, nextValue]]
        elif operator=="≥" and nextOperator=="<":
            return [["[", ")"], [nextValue, value]]

# TODO: combine rules
def combine_rules(gap,symbols):
    size = len(symbols)
    for index1, rule1 in gap.iterrows():
        for index2, rule2 in gap.iterrows():
            nb_diff = []
            for i in range(size):
                if rule1[i] != rule2[i]:
                    nb_diff.append[i]
            if len(nb_diff) == 1:
                j = nb_diff[0]
                new_rule = rule1
                new_rule[j][1]=rule1[j][1]+rule2[j][1]

def reverseOperator(operator):
    reversedOperator = ""
    if operator == "=":
        reversedOperator = "="
    if operator == "≥":
        reversedOperator = "<"
    if operator == "≤":
        reversedOperator = ">"
    if operator == ">":
        reversedOperator = "≤"
    if operator == "<":
        reversedOperator = "≥"
    if operator == "=":
        reversedOperator = "≠"
    return reversedOperator

def oppositeOperator(operator):
    reversedOperator = ""
    if operator == "≥":
        reversedOperator = "≤"
    if operator == "≤":
        reversedOperator = "≥"
    if operator == ">":
        reversedOperator = "<"
    if operator == "<":
        reversedOperator = ">"
    return reversedOperator

def refineIntCons(operator, value, nextOperator, nextValue):
    if str(operator) in ['>', '≥'] and nextOperator in ['>', '≥']:
        if value > nextValue:
            return [operator, value]
        elif value < nextValue:
            return [nextOperator, nextValue]
        elif value == nextValue and (operator == ">" or nextOperator == ">"):
            return [">",value]
        else:
            return ["≥",value]
    elif operator in ["<", "≤"] and nextOperator in ["<", "≤"]:
        if value > nextValue:
            return [nextOperator, str(nextValue)]
        elif value < nextValue:
            return [operator, value]
        elif value == nextValue and (operator == "<" or nextOperator == "<"):
            return ["<", value]
        else:
            return ["≤", value]
    elif operator in ["<", "≤"] and nextOperator in [">", "≥"]:
        if value < nextValue:
            return ["=","-"]
        elif operator in ["≤", "≥"] and nextOperator in ["≤", "≥"]:
            return ["[", str(nextValue), str(value),"]"]
        elif operator in ["<", ">"] and nextOperator in ["<", ">"]:
            return ["(", str(nextValue), str(value), ")"]
        elif operator == "≤" and nextOperator == ">":
            return ["(", str(nextValue), str(value), "]"]
        elif operator == "<" and nextOperator == "≥":
            return ["[", str(nextValue), str(value), ")"]
    elif operator in [">", "≥"] and nextOperator in ["<", "≤"]:
        if value > nextValue:
            return ["=","-"]
        elif operator in ["≤", "≥"] and nextOperator in ["≤", "≥"]:
            return ["[", str(value), str(nextValue), "]"]
        elif operator in ["<", ">"] and nextOperator in ["<", ">"]:
            return ["(", str(value), str(nextValue), ")"]
        elif operator == ">" and nextOperator == "≤":
            return ["(", str(value), str(nextValue), "]"]
        elif operator == "≥" and nextOperator == "<":
            return ["[", str(value), str(nextValue), ")"]
