import {  Component,  EventEmitter,  OnInit,  ViewChild,  ElementRef,  AfterViewInit,  ViewContainerRef} from '@angular/core';

// All DMN related imports.
import DmnModeler from 'dmn-js/dist/dmn-modeler.development.js';
import propertiesPanelModule from 'dmn-js-properties-panel';
import propertiesProviderModule from 'dmn-js-properties-panel/lib/provider/camunda';
import drdAdapterModule from 'dmn-js-properties-panel/lib/adapter/drd';
import * as camundaModdleDescriptor from 'camunda-dmn-moddle/resources/camunda.json';
import {migrateDiagram} from '@bpmn-io/dmn-migrate';

import {AppSettings} from '../../services/AppSettings';
import {IdpService} from '../../services/idp.service';

// Library to save files to disk.
import {saveAs} from 'file-saver';
@Component({
  selector: 'app-dmn-editor',
  templateUrl: './dmn-editor.component.html',
  styleUrls: ['./dmn-editor.component.css']
})

export class DmnEditorComponent implements OnInit, AfterViewInit {

  public tableStatus: string; // Whether or not the table is correct.
  private activeTable; // The table which is currently in view.
  private dmnDiagramModeler: DmnModeler; // DMN Modeller.
  private importDone: EventEmitter<any> = new EventEmitter(); // Fires as soon as  DMN Modeller, importing XML is success.
  public verificationOutput = null;

  @ViewChild('ref') private el: ElementRef; // host for Decision Diagram

  constructor(public idpService: IdpService) {
  }

  /**
   * Initializes the DMN editor. First, it creates the editor object.
   * We then set its 'on' event method to draw the modeller.
   * Afterwards, we import the DMN XML from the server into the modeller.
   */
  ngOnInit() {
    // Initialize DM Diagram Modeller.
    this.dmnDiagramModeler = new DmnModeler({
      keyboard: { bindTo: window },
      drd: {
        additionalModules: [propertiesPanelModule, propertiesProviderModule, drdAdapterModule]
      },
      moddleExtensions: {
        camunda: camundaModdleDescriptor
      },
      container: '#dmn-container' // This is the id of the html element we want to insert our modeller in.
    });

    // Set the IDP service's modeller.
    this.idpService.dmn_modeller = this.dmnDiagramModeler;

    // The following code fires when XML Import into DMN Modeller is complete.
    this.dmnDiagramModeler.on('import.done', ({ error, warnings }) => {
      console.log('Import Done event successfully emitted');
      if (!error) {
        // Draw the DMN model.
        const activeEditor = this.dmnDiagramModeler.getActiveViewer();
        const canvas = activeEditor.get('canvas');
        canvas.zoom('fit-viewport');
        activeEditor.attachTo(this.el.nativeElement);

        // DMN Canvas will have multiple views (one view for each decision box/table)
        const view = this.dmnDiagramModeler.getViews()[0];
        this.dmnDiagramModeler.open(view);


        // SWITCHING TABS
        this.dmnDiagramModeler.on('import.render.complete', (aNewView, err, wrngs) => {
        });
      } else {
        console.log(error);
      }
    });

    // Here we import the XML (and fire the above code), but only if there is no previous XML already present
    // (i.e. the object was not already initialized before)
    if (this.idpService.previousXML.length === 0) {
        const URL = AppSettings.DMN_SPEC_URL;
        this.idpService.loadDMN(URL);
        console.log(this.dmnDiagramModeler);
    } else {
        this.idpService.loadPreviousDMN();
    }
  }

  ngAfterViewInit() {
    this.dmnDiagramModeler.on('views.changed', event => {
      console.log('View Changed event got fired.....event = ', event);
      // Reset the table status when switching views.
      this.tableStatus = null;
      this.verificationOutput = null;

      // Trying to find elements to set their CSS.
      // console.log(this.dmnDiagramModeler._container.getElementsByTagName('tr')[3]);
      // console.log(this.dmnDiagramModeler._container.getElementsByTagName('tr')[4]);
      // console.log(this.dmnDiagramModeler._container.getElementsByTagName('tr')[5]);
      // console.log(this.dmnDiagramModeler._container.getElementsByClassName('rule-index'));
      // console.log(this.dmnDiagramModeler._container.getElementsByClassName('cell input-cell'));
      try {
        this.activeTable = event.activeView.element.decisionLogic;
      } catch (e) {
        this.activeTable = null;
      }
      // console.log(this.dmnDiagramModeler._container.getElementsByClassName('cell output-cell'));
      // console.log(this.dmnDiagramModeler._container.childNodes[0].childNodes[0].lastChild.children[1].childNode);
    });
  }

  /**
   * Upload a DMN file from disk to the interface.
   * @param event Contains the file.
   */
  uploadFile($event) {
    $event.target.files[0].text().then( x => {
        const xmlPromise = migrateDiagram(x);
        xmlPromise.then( xml => {
            this.dmnDiagramModeler.importXML(xml, (err, warnings) => {
                console.log('New XML uploaded!');
            });
        });
    });
  }

  /**
   * Download a DMN file from interface to disk.
   * Creates a DOMParser in order to parse the model's name from the XML.
   * @param event Not needed.
   */
  downloadFile($event) {
    this.idpService.downloadDMN();
  }

  /**
   * Reset the DMN model to an empty one. This empty model is hardcoded in.
   * @param event Not needed.
   */
  resetModel($event) {
    const emptyXml = `
    <?xml version="1.0" encoding="UTF-8"?>
    <definitions xmlns="https://www.omg.org/spec/DMN/20191111/MODEL/" xmlns:dmndi="https://www.omg.org/spec/DMN/20191111/DMNDI/"
        id="newModel" name="NewModel" namespace="http://camunda.org/schema/1.0/dmn" exporter="dmn-js (https://demo.bpmn.io/dmn)"
        exporterVersion="8.3.0">
    <dmndi:DMNDI>
    <dmndi:DMNDiagram id="DMNDiagram_05sfxgt" />
    </dmndi:DMNDI>
    </definitions>`;
    this.dmnDiagramModeler.importXML(emptyXml, (err, warnings) => {
        console.log('Reset XML');
        if (err) {
            console.log('Error', err);
        }
        if (warnings) {
            console.log('Warnings', warnings);
        }
    });
  }

  /**
   * Check a table for errors.
   * @param event Not needed.
   */
  checkModel($event) {
      let last_output = null;
      let tableId = null;
      let activeOutput = null;
      let activeHitPolicy = null;
      let activeTableRules = null;
      const inputs = [];
      try {
          last_output = this.activeTable.output.length - 1;
          tableId = this.activeTable.$parent.id;
          activeOutput = this.activeTable.output[last_output].name;
          for (let i = 0; i < this.activeTable.input.length; i++) {
              inputs.push(this.activeTable.input[i].inputExpression.text);
          }
          activeHitPolicy = this.activeTable.hitPolicy;
          activeTableRules = this.activeTable.rule.length;
      } catch (e) {
        this.tableStatus = 'Please open a table in order to check it for errors.';
        return;
      }

      // Reset the table status.
      this.tableStatus = '';
      const promises = [];

      // If one of the following hit policies, we can check the table.
      if (['UNIQUE', 'FIRST', 'ANY'].includes(activeHitPolicy)) {
          console.log(tableId);
          const overlap_status = this.idpService.errorCheck('overlap', tableId, inputs, activeTableRules);
          const shadow_status = this.idpService.errorCheck('shadowed', tableId, inputs, activeTableRules);
          const gap_status = this.idpService.errorCheck('gap', tableId, inputs, activeTableRules);
          promises.push(overlap_status);
          promises.push(shadow_status);
          promises.push(gap_status);
      } else {
        this.tableStatus = 'Can not check table with this hit policy.';
      }

      Promise.all(promises).then((values) => {
          for (let i = 0; i < values.length; i++) {
            if (values[i].error !== 'Table is correct.') {
                if (values[i].table.length > 0) {
                  this.verificationOutput = values[i].table;
                } else {
                  this.verificationOutput = null;
                }
                this.tableStatus = values[i].error;
                break;
            }
            if (i + 1 === values.length) {
                this.tableStatus = 'Table is correct';
                this.verificationOutput = null;
            }
          }
      });
  }
}
