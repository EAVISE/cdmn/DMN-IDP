# DMN-IDP

DMN-IDP is a full-fledged DMN tool combining the [dmn-js](https://github.com/bpmn-io/dmn-js) DMN editor and the IDP-based [interactive consultant interface](https://gitlab.com/krr/IDP-Z3).
For more information, view our [video demo](https://libre.video/videos/watch/3ce1e208-5399-4162-8c73-e24dd7597b4d) or read our publication [Vandevelde, Simon, and Vennekens, Joost. A Multifunctional, Interactive DMN Decision Modelling Tool. 2020.](https://www.researchgate.net/publication/346016725_A_Multifunctional_Interactive_DMN_Decision_Modelling_Tool)

We have also used this tool to demonstrate the importance of contex-aware verification of DMN tables.
This has also been presented at HICSS-55 2022: [Vandevelde, S., Callewaert, B., & Vennekens,  Joost. (2021). Context-aware verification of DMN](https://scholarspace.manoa.hawaii.edu/handle/10125/80096).

If you use any of these two works in your own, you may cite us as follows:

```
@inproceedings{Vandevelde2020,
	title = {A multifunctional, interactive {DMN} decision modelling tool},
	booktitle = {Proceedings of {BNAIC}/{BeneLearn} 2020},
	publisher = {Leiden University},
	author = {Vandevelde, Simon and Vennekens, Joost},
	year = {2020},
	pages = {399--400},
}

@inproceedings{Vandevelde2022,
journal = {Proceedings of the 55th Hawaii International Conference on System Sciences},
pages = {1--8},
publisher = {ScholarSpace},
isbn = {978-0-9981331-5-7},
year = {2022},
title = {Context-Aware verification of DMN},
author = {Vandevelde, Simon and Callewaert, Benjamin and Vennekens, joost},
}
```
